This is the source for https://emfhams.org/ (and https://staging.emfhams.org/ if
you are on the staging branch).

## How does this all work?

As Electromagnetic Field events are only every 2 years, changes probably don't
get made here very often, and you're probably struggling to remember what you
did 2 years ago.

On a git push to either master or staging, Gitlab CI will render the pages down
to HTML and push them to S3. See .gitlab-ci.yml for details.

Make a new branch for your work. If it's a minor edit then it's probably
acceptable to commit straight to the staging branch - but never master!

Use the 'dhugo' and 'dhugoserver' scripts to run the same Hugo in your local
Docker that Gitlab CI uses to build the site. Once you're happy that it looks
okay, merge your branch in to staging and check that everything looks good on
https://staging.emfhams.org/ . Only then should you merge staging in to master.
