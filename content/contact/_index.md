---
title: "Get in touch"
featured_image: '/img/contact-bg.jpg'
---

# IRC / Telegram

We're mostly using [Telegram][telegram] to organise the Amateur Radio Village,
but there is also a fairly inactive [IRC][irc] #emfhams channel on
[libera.chat][libera] or our [Telegram Group][telegramgroup] ([Join
now][jointelegram]).

[irc]: http://en.wikipedia.org/wiki/IRC
[telegram]: https://telegram.org/
[libera]: https://libera.chat
[jointelegram]: https://telegram.me/joinchat/A3glvAZ5xf_T9RkCfuDeOA 
[telegramgroup]: https://telegram.me/emfhams 

# Social Media
We're not very active, but we have a [twitter][twitter] account.

[fb]: https://www.facebook.com/groups/316141732057436/
[twitter]: https://twitter.com/emfhams
