---
title: "EMF Hams"
featured_image: '/img/intro-bg.jpg'
description: "Radio geeks in a field"
---
The EMF Hams group are a handful of Amateur Radio users who attend
[Electromagnetic Field][emfcamp] events to meet up and demonstrate our hobby to
others. If that sounds interesting to you, turn up with a tent and join our
village, or just drop in and say hello.

[emfcamp]: https://emfcamp.org/

We had a wonderful time at [EMF 2022](history/) and are now planning for 2024 -
we will be there, bigger than ever. We are combining forces with [CuTEL][cutel]
and [MastCar][mastcar] to form a larger village called [EMF Communications
Headquarters][echq]. Combined planning of all these groups is happening
[here][echq2024].

[cutel]: https://cutel.net/
[mastcar]: https://mastcar.uk/
[echq]: https://echq.org.uk/
[echq2024]: https://echq.org.uk/2024
