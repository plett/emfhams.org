---
title: "EMF Hams History"
featured_image: '/img/other-bg.jpg'
---

There has been an amateur radio contingent present at every [Electromagnetic
Field][emfcamp] event, gradually getting better organised as time goes on. I'm sure,
eventually, we'll get it right.

[emfcamp]: https://emfcamp.org/

## 2012

Milton Keynes. No formally organised group - just the natural overlap of EMF
attendees who happened to bring radios with them. There was also some
high-altitude balloon tracking.

## 2014

Milton Keynes. The first thing which could be described as an "Amateur Radio
Village" happened this year. Also, our special event call, GB2EMF, was first
issued for this event. We had a HF beam and a VHF/UHF vertical and demonstrated
amateur radio to anyone willing to stand still for long enough.

## 2016

Guildford. A lot more organised. As well as a special event call, we had an
on-site repeater, satellite tracking yagis, a HF beam on a 12 metre mast and a
beer fridge. We also experimented with web based contact logging and sent APRS
packets to the International Space Station.

## 2018

Eastnor. The [AMSAT UK][amsat] guys were in the neighbouring village, and they had *big*
antennas for satellite, so we focussed on other areas. Our repeater was upgraded to
be an internet-linked, multi-mode FM and DMR setup called [The Hub][hub]. We
also had an on-site APRS digipeater. The geography around the Eastnor site made
communication outside the site challenging, but some of us climbed a nearby hill
to take part in a VHF contest.

[amsat]: https://amsat-uk.org/
[hub]: http://hub.emfhams.org/


## 2020

EMF 2020 was unfortunately [cancelled][emf2020cancel] due to COVID-19.
Before it was cancelled, the following amateur radio activities were planned for
the event:

* Foundation licence exams and practical assessments for up to maybe a dozen
  people
* A UHF analogue repeater with the callsign GB3MF
* The return of [The Hub][hub] FM and DMR internet-linked gateway/repeater
* The same APRS IGate and Digipeater as in 2018
* AMSAT-UK were [scheduled to talk with astronauts][amsatiss2020] on the
  International Space Station

[emf2020cancel]: https://blog.emfcamp.org/2020/03/25/emf-2020-cancelled/
[amsatiss2020]: https://amsat-uk.org/2020/02/07/emf-2020-iss-contact/

## 2022

After missing 2020, EMF 2022 was bigger and better than ever with many different
groups bringing equipment for many different modes.

There was:

* Our new G1EMF club callsign working as many bands and modes as we could manage
* [MastCar](https://mastcar.uk/) providing a 10m pump up mast to hold our antennas
* GB3MF 70cm analogue repeater run by M0TRY
* GB7NQ 70cm DMR repeater run by Mal M0VNA from Maker Space in Newcastle
* An AllStar node with a 2m simplex access port run by Jim M0ZAH
* 2m analogue APRS run by Paul M0PLL
* MB7ULG 70cm LoRA APRS run by Mal M0VNA from Maker Space in Newcastle
* MB7PMF 70cm POCSAG pager transmitter attached to DAPNET. Also run by Newcastle Maker Space
* AMSAT UK had their own marquee, 40 foot mast for UHF/VHF and 2.4m dish for
  DATV and narrowband via QO-100

We also hosted the core of the [CuTEL][cutel2022] network in our village providing
physical copper phone lines to provide phone calls, fax and dialup across the
camp.

[cutel2022]: https://www.cutel.net/emf_2022/

With all these different modes, we made sure to try and link as many of them
together as possible. We had text messaging working between DMR radios, analogue
APRS radios, LoRA APRS and pagers. We also had a phone patch set up on the
AllStar node and were able to have [a QSO between a radio on the AllStar node
and an analogue phone][allstarcall] on a CuTEL line installed to a campervan.

[allstarcall]: https://www.youtube.com/shorts/wmc80XMApR4

An idea that we didn't manage to implement in time was an SSTV to fax gateway.
Maybe we'll have a go at that one in 2024!
